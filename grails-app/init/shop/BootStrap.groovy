package shop
import com.icthost.sms.library.UOM;
import com.icthost.sms.library.ProductGroup;
class BootStrap {

    def init = { servletContext ->
    	
    	//add some UOM
    	new UOM(unitName:"Pcs").save()
    	new UOM(unitName:"Set").save()
    	new UOM(unitName:"Kgs").save()
    	new UOM(unitName:"Ltr").save()
    	new UOM(unitName:"Gallon").save()

    	//add some Product group
    	new ProductGroup(groupName:"Stationary").save()
    	new ProductGroup(groupName:"Electronics").save()
    	new ProductGroup(groupName:"IT Accessories").save()
    	new ProductGroup(groupName:"Food Item").save()
    
    }
    def destroy = {
    }
}
