package com.icthost.sms.library

class Product {
	String name
	String description
	String toString(){
		"$name"
	}
    static constraints = {
    	name (blank: false,unique:true)
    	description nullable:true
    }
    static belongsTo=[productGroup:ProductGroup,uOM:UOM]

}
