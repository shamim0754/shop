package com.icthost.sms.library

class UOM {
	String unitName
	String toString(){
		"$unitName"
	}
    static constraints = {
    	unitName(blank:false)
    }
    static hasMany=[product:Product]
}
