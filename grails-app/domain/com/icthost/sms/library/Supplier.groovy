package com.icthost.sms.library

class Supplier {
	String name
	String address
	String mobile
	Date createDate=new Date()
    static constraints = {
    	name (blank: false,unique:true)
    	address (blank: false)
    	mobile (nullable: true,range:1..13)
    }
}
