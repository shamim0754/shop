package com.icthost.sms.library

class ProductGroup {
	String groupName
	String toString(){
		"$groupName"
	}
    static constraints = {
    	groupName (blank: false,unique:true)
    }
    static hasMany=[products:Product]
}
